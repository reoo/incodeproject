package com.incode.android.incodeinterview.description;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by reoo on 12/16/16.
 */

public interface DescriptionPresenter {

    void getPhotoDescription();

    void onDestroy();

    void storagePermissionDenied();

    void askForPermission();

    void savePhoto(File parent, Bitmap bitmap);

    void extractShareData();

    void startIntent(File file);


}
