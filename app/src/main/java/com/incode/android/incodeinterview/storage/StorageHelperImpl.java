package com.incode.android.incodeinterview.storage;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by reoo on 12/17/16.
 */

public class StorageHelperImpl implements StorageHelper {
    private static final String directoryName = "incodemedia";

    @Override
    public File createFile(File parent, String nameSuffix) {
        //final String imageName = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date()) + ".jpg";
        final File photosDirectory = new File(parent, directoryName);
        photosDirectory.mkdirs();
        final File image = new File(photosDirectory, nameSuffix + ".jpg");
        return image;
    }

    @Override
    public void save(File parent, Bitmap bitmap, OnStorageListener listener) {
        if(bitmap == null) {
            throw new IllegalArgumentException("bitmap can not be null");
        }

        //File photosDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "incodemedia");
        //photosDirectory.mkdirs();
        final File photosDirectory = new File(parent, directoryName);
        photosDirectory.mkdirs();
        File file = new File(photosDirectory, (new Date().toString()) +  ".jpg");
        FileOutputStream outputStream;

        try {
            outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
            outputStream.flush();
            outputStream.close();
            //String location = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "incode.jpg", "for the interview project");
            listener.onSuccess(file);
        } catch (IOException e) {
            listener.onError();
            e.printStackTrace();
        }
    }

}
