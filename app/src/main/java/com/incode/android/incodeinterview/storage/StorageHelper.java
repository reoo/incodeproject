package com.incode.android.incodeinterview.storage;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by reoo on 12/17/16.
 */

public interface StorageHelper {

    /**
     * Create a new jpg file in the parent location with name suffix
     * @param parent Parent directory
     * @param nameSuffix File name suffix. eg. myImage for a file myImage.jpg
     * @return the new File created
     */
    File createFile(File parent, String nameSuffix);

    /**
     * Save a bitmap in the parent location
     * @param parent Parent directory
     * @param bitmap Bitmap to save
     * @param listener will be informed of the result
     */
    void save(File parent, Bitmap bitmap, OnStorageListener listener);

    interface OnStorageListener {

        /**
         * File created succesful
         * @param file
         */
        void onSuccess(File file);

        /**
         * Error creating a new file
         */
        void onError();
    }

}
