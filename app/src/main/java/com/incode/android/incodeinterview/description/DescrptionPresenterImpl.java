package com.incode.android.incodeinterview.description;

import android.graphics.Bitmap;

import com.incode.android.incodeinterview.storage.StorageHelper;
import com.incode.android.incodeinterview.storage.StorageHelperImpl;

import java.io.File;

/**
 * Created by reoo on 12/16/16.
 */

public class DescrptionPresenterImpl implements DescriptionPresenter, StorageHelper.OnStorageListener {
    private DescriptionView descriptionView;
    private StorageHelper storageHelper;

    public DescrptionPresenterImpl(DescriptionView descriptionView) {
        this.descriptionView = descriptionView;
        this.storageHelper = new StorageHelperImpl();
    }

    @Override
    public void getPhotoDescription() {
        checkRep();

        descriptionView.showComment(descriptionView.getPhotoComment());
        descriptionView.showPhoto(descriptionView.getPhotoUrl());
    }

    @Override
    public void onDestroy() {
        descriptionView = null;
    }

    @Override
    public void storagePermissionDenied() {
        checkRep();

        descriptionView.showStoragePermissionDeniedMessage();
    }

    @Override
    public void askForPermission() {
        checkRep();

        descriptionView.askForPermission();
    }

    @Override
    public void savePhoto(File parent, Bitmap bitmap) {
        if(storageHelper == null)
            throw new IllegalArgumentException("storage helper can not be null");

        storageHelper.save(parent, bitmap, this);
    }

    @Override
    public void extractShareData() {
        checkRep();

        descriptionView.extractDataToShare();
    }

    @Override
    public void startIntent(File file) {
        checkRep();

        descriptionView.createShareIntent(file);
    }

    @Override
    public void onSuccess(File file) {
        checkRep();

        descriptionView.createShareIntent(file);
    }

    @Override
    public void onError() {
        checkRep();
        descriptionView.showStorageError();
    }

    private void checkRep() {
        if(descriptionView == null)
            throw new IllegalArgumentException("view can not be null");
    }

}
