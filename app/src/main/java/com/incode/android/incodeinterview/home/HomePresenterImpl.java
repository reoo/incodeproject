package com.incode.android.incodeinterview.home;

import com.incode.android.incodeinterview.MainActivity;
import com.incode.android.incodeinterview.model.Photo;
import com.incode.android.incodeinterview.storage.StorageHelper;
import com.incode.android.incodeinterview.storage.StorageHelperImpl;

import java.io.File;
import java.util.List;

/**
 * Created by reoo on 12/15/16.
 */

public class HomePresenterImpl implements HomePresenter, HomeInteractor.OnPhotoFetchFinishedListener {
    private static final String TAG = "HomePresenterImpl";
    private HomeView homeView;
    private HomeInteractor homeInteractor;
    private StorageHelper storageHelper;

    public HomePresenterImpl(HomeView homeView) {
        this.homeView = homeView;
        this.homeInteractor = new HomeInteractorImpl();
        this.storageHelper = new StorageHelperImpl();
    }

    @Override
    public void getPhotos() {
        if(homeView != null)
            homeView.showProgressIndicator();

        homeInteractor.fetchPhotos(this);
    }

    @Override
    public void onDestroy() {
        homeView = null;
    }

    @Override
    public void askForPhotoDetail(Photo photo) {
        homeView.goToPhotoDetail(photo);
    }

    @Override
    public void askForStoragePermission() {
        if(homeView == null)
            throw new IllegalArgumentException("view can not be null");

        homeView.askForStoragePermission();
    }

    @Override
    public void storagePermissionDenied() {
        if(homeView == null)
            throw new IllegalArgumentException("view can not be null");

        homeView.showStoragePermissionDeniedMessage();
    }

    @Override
    public void openCamera() {
        if(homeView == null)
            throw new IllegalArgumentException("view can not be null");

        homeView.prepareCamera();
    }

    @Override
    public File getPhoto(File parent, String nameSuffix) {
        return this.storageHelper.createFile(parent, nameSuffix);
    }

    @Override
    public void addPhoto(Photo photo) {
        if(homeView == null)
            throw new IllegalArgumentException("view can not be null");

        homeView.addPhotoToList(photo);
    }

    @Override
    public void onSuccess(final List<Photo> photos) {
        if(homeView != null) {
            ((MainActivity) homeView).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    homeView.hideProgressIndicator();
                    homeView.showPhotos(photos);
                    homeView.showCameraButton();
                }
            });
        }
    }

    @Override
    public void onError() {
        if(homeView != null) {
            ((MainActivity) homeView).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    homeView.showNetworkErrorMessage();
                    homeView.hideProgressIndicator();
                }
            });
        }

    }

}
