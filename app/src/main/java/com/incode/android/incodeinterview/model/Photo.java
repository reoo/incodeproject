package com.incode.android.incodeinterview.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by reoo on 12/15/16.
 */

public class Photo {
    private final String url;
    private final String comment;
    private final Date date;

    public Photo(String url, String comment, Date date) {
        this.url = url;
        this.comment = comment;
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public String getComment() {
        return comment;
    }

    public Date getDate() {
        return date;
    }

    public static List<Photo> toList(String json) throws JSONException {
        ArrayList<Photo> list = new ArrayList<>();
        JSONArray array = new JSONArray(json);
        int n = array.length();
        for(int i = 0; i < n; i++) {
            final JSONObject item = array.getJSONObject(i);
            final String url = (item.has("picture")) ? item.getString("picture"): "";
            final String comment = (item.has("comment")) ? item.getString("comment"): "";
            final String publishedAt = (item.has("publishedAt")) ? item.getString("publishedAt"): "";
            Photo photo = new Photo(url, comment, new Date(publishedAt));
            list.add(photo);
        }
        Collections.sort(list, new Comparator<Photo>() {
            @Override
            public int compare(Photo photo, Photo t1) {
                return t1.date.compareTo(photo.date);
            }
        });
        return list;
    }

}
