package com.incode.android.incodeinterview.description;

import java.io.File;

/**
 * Created by reoo on 12/16/16.
 */

public interface DescriptionView {

    void showComment(String c);

    void showPhoto(String u);

    void showStorageError();

    void showStoragePermissionDeniedMessage();

    void showErrorMessage(String message);

    String getPhotoUrl();

    String getPhotoComment();

    void askForPermission();

    void extractDataToShare();

    void createShareIntent(File file);

}
