package com.incode.android.incodeinterview.home;

import com.incode.android.incodeinterview.model.Photo;

import java.io.File;

/**
 * Created by reoo on 12/15/16.
 */

public interface HomePresenter {

    void getPhotos();

    void onDestroy();

    void askForPhotoDetail(Photo photo);

    void askForStoragePermission();

    void storagePermissionDenied();

    void openCamera();

    File getPhoto(File parent, String nameSuffix);

    void addPhoto(Photo photo);

}
