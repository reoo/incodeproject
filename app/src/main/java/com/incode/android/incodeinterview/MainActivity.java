package com.incode.android.incodeinterview;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenu;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.incode.android.incodeinterview.adapter.PhotoAdapter;
import com.incode.android.incodeinterview.description.DescriptionActivity;
import com.incode.android.incodeinterview.home.HomePresenter;
import com.incode.android.incodeinterview.home.HomePresenterImpl;
import com.incode.android.incodeinterview.home.HomeView;
import com.incode.android.incodeinterview.model.LocalPhoto;
import com.incode.android.incodeinterview.model.Photo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements HomeView {
    private static final String TAG = "HomeActivity";
    private ProgressBar progressBar;
    private TextView emptyState;
    private TextView networkError;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PhotoAdapter photoAdapter;

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    private HomePresenter homePresenter;

    private LocalPhoto localPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progress_indicator_pb);
        emptyState = (TextView) findViewById(R.id.empty_state_tv);
        networkError = (TextView) findViewById(R.id.network_error_tv);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        homePresenter = new HomePresenterImpl(this);
        homePresenter.getPhotos();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homePresenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity, menu);
        MenuItem item = menu.findItem(R.id.action_take_a_picture);
        if(photoAdapter == null) {
            item.setVisible(false);
        } else {
            item.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_take_a_picture:
                homePresenter.askForStoragePermission();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            // if there wasn't taken any photo the new file is not created
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(new File(localPhoto.getUrl())));
            this.sendBroadcast(mediaScanIntent);
            homePresenter.addPhoto(localPhoto);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                homePresenter.openCamera();
            } else {
                homePresenter.storagePermissionDenied();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showProgressIndicator() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressIndicator() {
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyState() {
        this.emptyState.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyState() {
        this.emptyState.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage() {
        Snackbar.make(this.progressBar, R.string.something_went_wrong, Snackbar.LENGTH_INDEFINITE)
                .show();;
    }

    @Override
    public void showNetworkErrorMessage() {
        this.networkError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showCameraButton() {
        invalidateOptionsMenu();
    }

    @Override
    public void goToPhotoDetail(Photo photo) {
        Intent intent = new Intent(this, DescriptionActivity.class);
        intent.putExtra("comment", photo.getComment());
        intent.putExtra("url", photo.getUrl());
        startActivity(intent);
    }

    @Override
    public void showPhotos(List<Photo> photoList) {
        photoAdapter = new PhotoAdapter(photoList);
        photoAdapter.setItemClickListener(new PhotoAdapter.PhotoItemClickListener() {
            @Override
            public void onItemClick(Photo photo, int position) {
                homePresenter.askForPhotoDetail(photo);
            }
        });
        recyclerView.setAdapter(photoAdapter);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showUseOfStoragePermission() {
        Snackbar.make(this.progressBar, R.string.we_need_to_save_the_image_before_share_it,
                Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showStoragePermissionDeniedMessage() {
        Snackbar.make(this.progressBar, R.string.without_the_storage_permission_we_cant_take_picture,
                Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void askForStoragePermission() {
        final String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(this, permission)
                == PackageManager.PERMISSION_GRANTED) {

            homePresenter.openCamera();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                homePresenter.storagePermissionDenied();
            }
            ActivityCompat.requestPermissions(this,
                    new String[]{permission}, REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void prepareCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File picturesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        final Date date = new Date();
        File image = homePresenter.getPhoto(picturesDir, date.toString());
        localPhoto = new LocalPhoto(image.getAbsolutePath(), date);
        if (intent.resolveActivity(getPackageManager()) != null) {
            // if there wasn't taken any photo the new file is not created
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void addPhotoToList(Photo photo) {
        photoAdapter.addNewItem(photo);
        recyclerView.getAdapter().notifyItemInserted(0);
        photoAdapter.notifyDataSetChanged();
    }

}
