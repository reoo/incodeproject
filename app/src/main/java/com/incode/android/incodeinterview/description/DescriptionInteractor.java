package com.incode.android.incodeinterview.description;

/**
 * Created by reoo on 12/16/16.
 */

public interface DescriptionInteractor {

    interface OnPhotoDescriptionFinishedListener {
        void onSuccess();

        void onError();
    }

    void fetchPhoto(String url, OnPhotoDescriptionFinishedListener listener);

}
