package com.incode.android.incodeinterview.model;

import java.util.Date;

/**
 * Created by reoo on 12/17/16.
 */

public class LocalPhoto extends Photo {

    public LocalPhoto(String url, Date date) {
        super(url, null, new Date(date.toString()));
    }

    @Override
    public String getComment() {
        return "comments not supported";
    }
}
