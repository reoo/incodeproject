package com.incode.android.incodeinterview.home;

import com.incode.android.incodeinterview.model.Photo;

import java.util.List;

/**
 * Created by reoo on 12/15/16.
 */

public interface HomeView {

    /**
     * Make the progress indicator visible
     */
    void showProgressIndicator();

    /**
     * Hide the progress indicator
     */
    void hideProgressIndicator();

    /**
     * Make empty state feedback view visible
     */
    void showEmptyState();

    /**
     * Hide the empty state feedback
     */
    void hideEmptyState();

    /**
     * Display error message
     */
    void showErrorMessage();

    /**
     * Display error message caused by network issues
     */
    void showNetworkErrorMessage();

    /**
     * Display camera button
     */
    void showCameraButton();

    /**
     * Navigates to the photo detail view
     */
    void goToPhotoDetail(Photo photo);

    /**
     * Display photos from remote file
     */
    void showPhotos(List<Photo> photoList);

    /**
     * Display information of the use of storage permission
     */
    void showUseOfStoragePermission();

    /**
     * Display permission storage error
     */
    void showStoragePermissionDeniedMessage();

    /**
     * Ask for for the storage permission
     */
    void askForStoragePermission();

    /**
     * Open the camera to take a new photo
     */
    void prepareCamera();

    /**
     * Add a new photo object to the list rendered
     */
    void addPhotoToList(Photo photo);

}
