package com.incode.android.incodeinterview.description;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.incode.android.incodeinterview.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class DescriptionActivity extends AppCompatActivity implements DescriptionView {
    private static final String TAG = "DescriptionActivity";
    private ImageView photo;
    private TextView comment;
    private FloatingActionButton share;

    private DescriptionPresenter descriptionPresenter;

private static final int WRITE_EXTERNAL_PERMISSION_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        photo = (ImageView) findViewById(R.id.photo_iv);
        comment = (TextView) findViewById(R.id.photo_comment_tv);
        share = (FloatingActionButton) findViewById(R.id.fab);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descriptionPresenter.askForPermission();
            }
        });

        descriptionPresenter = new DescrptionPresenterImpl(this);
        descriptionPresenter.getPhotoDescription();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        descriptionPresenter.onDestroy();
    }

    @Override
    public void showComment(String c) {
        comment.setText(c);
        comment.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPhoto(String u) {
        Glide.with(this).load(u).into(photo);
        photo.setVisibility(View.VISIBLE);
    }

    @Override
    public void showStorageError() {
        Snackbar.make(photo, R.string.something_went_wrong, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showStoragePermissionDeniedMessage() {
        Snackbar.make(photo, R.string.we_need_to_save_the_image_before_share_it,
                Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(photo, message, Snackbar.LENGTH_INDEFINITE).show();
    }

    @Override
    public void askForPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {

            descriptionPresenter.extractShareData();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                descriptionPresenter.storagePermissionDenied();
            }
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_PERMISSION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == WRITE_EXTERNAL_PERMISSION_REQUEST) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                descriptionPresenter.extractShareData();
            } else {
                descriptionPresenter.storagePermissionDenied();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public String getPhotoUrl() {
        return getIntent().getExtras().getString("url");
    }

    @Override
    public String getPhotoComment() {
        return getIntent().getExtras().getString("comment");
    }

    @Override
    public void createShareIntent(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        this.sendBroadcast(mediaScanIntent);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/jpeg");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        startActivity(intent);
    }

    @Override
    public void extractDataToShare() {
        Bitmap bitmap = ((GlideBitmapDrawable) photo.getDrawable()).getBitmap();
        if(bitmap == null) {
            showErrorMessage(getString(R.string.something_went_wrong));
            return;
        }
        File picturesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        descriptionPresenter.savePhoto(picturesDir, bitmap);
    }

}
