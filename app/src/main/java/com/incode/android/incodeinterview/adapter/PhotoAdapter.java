package com.incode.android.incodeinterview.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.incode.android.incodeinterview.R;
import com.incode.android.incodeinterview.model.LocalPhoto;
import com.incode.android.incodeinterview.model.Photo;

import java.util.List;

/**
 * Created by reoo on 12/15/16.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {
    private List<Photo> photos;
    private PhotoItemClickListener itemClickListener;

    public PhotoAdapter(List<Photo> photos) {
        this.photos = photos;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new PhotoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, final int position) {
        Glide.with(holder.view.getContext())
                .load(photos.get(position).getUrl())
                //.placeholder(android.R.drawable.progress_horizontal)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into((ImageView) holder.view.findViewById(R.id.item_photo_iv));
        ((TextView) holder.view.findViewById(R.id.date_tv)).setText(photos.get(position).getDate().toString());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(itemClickListener != null)
                    itemClickListener.onItemClick(photos.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.photos.size();
    }

    public void setItemClickListener(PhotoItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void addNewItem(Photo photo) {
        this.photos.add(0, new LocalPhoto(photo.getUrl(), photo.getDate()));
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        View view;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
        }
    }

    public interface PhotoItemClickListener {
        void onItemClick(Photo photo, int position);
    }

}
