package com.incode.android.incodeinterview.home;

import com.incode.android.incodeinterview.model.Photo;

import java.io.IOException;
import java.util.List;

/**
 * Created by reoo on 12/15/16.
 */

public interface HomeInteractor {

    interface OnPhotoFetchFinishedListener {
        void onSuccess(List<Photo> photos);

        void onError();
    }

    void fetchPhotos(OnPhotoFetchFinishedListener listener);

}
