package com.incode.android.incodeinterview.home;

import com.incode.android.incodeinterview.model.Photo;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by reoo on 12/15/16.
 */

public class HomeInteractorImpl implements HomeInteractor {

    @Override
    public void fetchPhotos(final OnPhotoFetchFinishedListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://beta.json-generator.com/api/json/get/EkphH5xyM")
                        .build();
                Response response = null;
                String json = null;
                try {
                    response = client.newCall(request).execute();
                    json = response.body().string();
                    listener.onSuccess(Photo.toList(json));
                } catch (IOException e) {
                    e.printStackTrace();
                    listener.onError();
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError();
                }
            }
        }).start();
    }

}
